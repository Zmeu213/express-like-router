const { match } = require('node-match-path')

class Router {
  constructor () {
    this._use = {}
    this._get = {}
    this._post = {}
  }

  use (path, middleware) {
    if (!middleware && typeof path === 'function') {
      middleware = path
      path = '/'
    }
    if (!this._use[path]) {
      this._use[path] = []
    }
    this._use[path].push(middleware)
  }

  get (path, middleware) {
    if (!middleware && typeof path === 'function') {
      middleware = path
      path = '/'
    }
    this._get[path] = middleware
  }

  post (path, middleware) {
    if (!middleware && typeof path === 'function') {
      middleware = path
      path = '/'
    }
    this._post[path] = middleware
  }

  serve () {
    const getMiddleware = (set, requestPath) => {
      const found = Object.keys(set)
        .map(path => ({ ...match(path, requestPath), path }))
        .filter(res => res.matches)
        .sort((resA, resB) => Object.keys(resA.params || {}).length - Object.keys(resB.params || {}).length)
        .shift()
      if (found) {
        return set[found.path]
      } else {
        return null
      }
    }
    return async (request, response) => {
      const useMiddlewares = getMiddleware(this._use, request.url)
      if (useMiddlewares && useMiddlewares.length >= 1) {
        for (const func of useMiddlewares) {
          const promise = new Promise(resolve => func(request, response, resolve))
          await promise
        }
      }
      if (request.method === 'GET') {
        const middleware = getMiddleware(this._get, request.url)
        middleware && middleware(request, response)
      } else if (request.method === 'POST') {
        const middleware = getMiddleware(this._post, request.url)
        middleware && middleware(request, response)
      } else {
        return request.end('Unsupported method')
      }
    }
  }
}

module.exports = Router
