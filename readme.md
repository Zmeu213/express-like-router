# The `router`

## What is it

This is a express-like custom router module for registering middlewares and execution them while transfering ownership. This repo contain `router.js` with router itself and `index.js` with example usage of module. Router only depends on `node-match-path` because im lazy and dont whant to implement own pattern matching.

## How to run

`git clone repo`

`npm i`

`node index.js`

`curl localhost:3000/`